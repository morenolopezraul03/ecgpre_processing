% Loading signal
data = load('ecgConditioningExample.mat');
fs = data.fs;
ECG = data.ecg;

% Checking channel connections
for i = 1:size(ECG, 2)
    if ECG(:,i) == zeros(length(ECG),1)
        disp(['ERROR: channel ' mat2str(i) ' disconnected'])
    end
end

% Signal visualization: before pre-processing
figure
sgtitle('Raw signals')
for i = 1:size(ECG, 2)
    subplot(3, 2, i)
    plot(linspace(0,(length(ECG)/data.fs),length(ECG)),ECG(:,i))
    title(['ECG signal ' mat2str(i)])
    ylabel('ECG')
    xlabel('Time (s)')
end

% Recording spikes removal
for i = 1:size(ECG, 2)
    % Recording spikes localization
    [pks, locs] = findpeaks(ECG(:,i));
    artifacts = locs(pks>2*mean(ECG(:,i)));
    
    % Artifacts removal: signal interpolation in a +/- 10 window
    w = 10; % Window size
    if isempty(artifacts) == 0
        xq = [1:length(ECG(:,i))]';
        for j = 1:length(artifacts)
            x = [1:artifacts(j)-w, artifacts(j)+w:length(ECG(:,i))]';
            v = [ECG(1:artifacts(j)-w,i)', ECG(artifacts(j)+w:end,i)']';
            ECG(:,i) = interp1(x,v,xq);
        end
    end
end

% Filtering
% Cut-off frequencies (Hz)
cf_high = 0.5;
cf_low = 70;
cf_notch = 50;

% High-pass filtering: baseline wander correction.
[b1,a1]=butter(5,(cf_high/(data.fs/2)),'high');  

% Low-pass filtering: elimination of noise from high frequencies.
[b2,a2]=butter(5,(cf_low/(data.fs/2)),'low');

% Notch filter: powerline interference filtering. 
wo = cf_notch/(fs/2);
bw = wo/35;
[b3,a3] = iirnotch(wo,bw); 

% A second notch filter is applied as harmonics have been detected at 100 Hz
% when inspecting the frequency domain
wo = 100/(fs/2);
bw = wo/35;
[b4,a4] = iirnotch(wo,bw); 

for i = 1:size(ECG,2)
    ECG(:,i) = filtfilt(b1,a1,ECG(:,i));
    ECG(:,i) = filtfilt(b2,a2,ECG(:,i));
    ECG(:,i) = filtfilt(b3,a3,ECG(:,i));
    ECG(:,i) = filtfilt(b4,a4,ECG(:,i));
end 

% Offset removal
for i = 1:size(ECG,2)
   ECG(:,i) = ECG(:,i) - mean(ECG(:,i)); 
end

% Signal visualization: after pre-processing
figure
sgtitle('Pre-processing applied') 
for i = 1:size(ECG, 2)
    subplot(3, 2, i)
    plot(linspace(0,(length(ECG)/data.fs),length(ECG)),ECG(:,i))
    title(['ECG signal ' mat2str(i)])
    ylabel('ECG')
    xlabel('Time (s)')
    axis([0 (length(ECG)/data.fs) min(ECG,[],'all') max(ECG,[],'all')])
end

% Note: Some distortions are found after filtering where artifacts were
% found. These could me attenuated by applying a lower cut-off frequency 
% for the low-pass filter (for instance, 40 Hz). However, such a 
% restrictive filter could eliminate important information about the QRS 
% complexes.
